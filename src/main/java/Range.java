public class Range {
  private static final int MAX_RANGE = 100;
  private int upBoundary;
  private int downBoundary;

  public Range(int upBoundary, int downBoundary) {
    this.upBoundary = upBoundary;
    this.downBoundary = downBoundary;
  }

  public Range() {
  }

  public int getUpBoundary() {
    return upBoundary;
  }

  public int getDownBoundary() {
    return downBoundary;
  }

  private boolean isUpValid(int down, int up) {
    return ((up <= MAX_RANGE) && (down < up));
  };

  private boolean isDownValid(int down) {
    return ((down >= 1) && (down <= MAX_RANGE - 1));
  };

  public Range createRange() {

    ConsoleListener console = new ConsoleListener();
    int down;
    do {
      System.out.println("Enter down range (integer number should be from [1,99])");
      down = console.getInteger();
    } while (!isDownValid(down));

    int up;
    do {
      System.out.println(
          "Enter up range (integer number should be greater than down range and from [2,100])");
      up = console.getInteger();
    } while (!isUpValid(down, up));
    return new Range(up, down);
  }

  @Override
  public String toString() {
    return "Range{" + "downBoundary=" + downBoundary + ", upBoundary=" + upBoundary + '}';
  }
}
