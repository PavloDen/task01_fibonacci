import java.util.ArrayList;
import java.util.List;

public class Calculator {

  public int getSum(List<Integer> list) {
    int sum = 0;
    for (Integer l : list) {
      sum = sum + l;
    }
    return sum;
  }

  public int getFibonacciNumber(int n) {
    if (n <= 1) {
      return n;
    } else {
      return getFibonacciNumber(n - 1) + getFibonacciNumber(n - 2);
    }
  }

  public List<Integer> getEvenNumbers(Range range) {
    List<Integer> evenNumbers = new ArrayList();
    for (int i = range.getDownBoundary(); i <= range.getUpBoundary(); i++) {
      if ((i % 2) == 0) {
        evenNumbers.add(i);
      }
    }
    return evenNumbers;
  }

  public List<Integer> getOddNumbers(Range range) {
    List<Integer> oddNumbers = new ArrayList();
    for (int i = range.getDownBoundary(); i <= range.getUpBoundary(); i++) {
      if ((i % 2) != 0) {
        oddNumbers.add(i);
      }
    }
    return oddNumbers;
  }

}
