//   Write program, which will pass requirements:
//      - User enter the interval (for example: [1;100]);
//      - Program prints odd numbers from start to the end of interval
//        and even from end to start;
//      - Program prints the sum of odd and even numbers;
//      - Program build Fibonacci numbers: F1 will be the biggest odd number
//        and F2 – the biggest even number, user can enter the size of set (N);

import java.util.Collections;
import java.util.List;

public class App {

  public static void main(String[] args) {

    Range range = new Range();
    range = range.createRange();
    System.out.println("Created range " + range);

    Calculator calculator = new Calculator();
    List<Integer> evenNumbers = calculator.getEvenNumbers(range);
    System.out.println("Even numbers from end to start " + evenNumbers);
    System.out.println("Sum of even numbers =" + calculator.getSum(evenNumbers));

    List<Integer> oddNumbers = calculator.getOddNumbers(range);
    Collections.reverse(oddNumbers);
    System.out.println("Odd numbers from start to end " + oddNumbers);
    System.out.println("Sum of odd numbers =" + calculator.getSum(oddNumbers));

    System.out.println("Input size of set n (integer) ");
    ConsoleListener console = new ConsoleListener();
    int sequenceSize = console.getInteger();

    System.out.println("Fibonacci numbers from biggest even number");
    int biggestEvenNumber = Collections.max(evenNumbers);
    printFibonacciNumbers(biggestEvenNumber, sequenceSize);

    System.out.println("Fibonacci numbers from biggest odd number");
    int biggestOddNumber = Collections.max(oddNumbers);
    printFibonacciNumbers(biggestOddNumber, sequenceSize);
  }

  public static void printFibonacciNumbers(int number, int size) {
    Calculator calculator = new Calculator();
    for (int i = number; i <= number + size; i++) {
      System.out.println("  " + i + " : " + calculator.getFibonacciNumber(i));
    }
  }
}
