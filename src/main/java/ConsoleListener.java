import org.apache.commons.lang.math.NumberUtils;

import java.util.Scanner;

public class ConsoleListener {

  public Integer getInteger() {
    Scanner keyboard = new Scanner(System.in);
    String strInput = keyboard.next();
    return NumberUtils.createInteger(strInput);
  }
}
